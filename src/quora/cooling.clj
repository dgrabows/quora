(ns quora.cooling
  (:require [clojure.string :as s]
            [clojure.set :as set]
            [clojure.pprint :as pp]))

;;;; main solution

(defn ans-length
  "Calculates the path length of correct answers."
  [grid]
  (+ 2
    (count
      (filter #{:ow} (flatten grid)))))

(defn find-room [grid key]
  (first
    (filter #(not (neg? (get % 1)))
            (map vector
                 (range (count grid))
                 (map #(.indexOf % key) grid)))))

(defn mark-reachable [g pos]
  (let [[row col] pos
        room (get-in g pos)]
    (case room
      (:in :ac :ow ) (-> (assoc-in g pos :re )
                       (mark-reachable [row (dec col)])
                       (mark-reachable [row (inc col)])
                       (mark-reachable [(dec row) col])
                       (mark-reachable [(inc row) col]))
      g)))

(defn connected? [g]
  (let [ac (find-room g :ac)
        reachable (mark-reachable g ac)]
    (not
      (some #{:in :ac :ow } (flatten reachable)))))

(defn neighbors
  "Returns sequence of neighbor positions in left, right, up, down order."
  [g [row col]]
  [[row (dec col)]
   [row (inc col)]
   [(dec row) col]
   [(inc row) col]])

(defn deadend? [g pos]
  (and
    (= :ow (get-in g pos))
    (= 1 (count (filter #{:ow :in :ac} (map (partial get-in g) (neighbors g pos)))))))

(defn find-deadends [g positions]
  (filter (partial deadend? g) positions))

(defn prune-deadends
  "Returns a sequence of the rooms neighboring pos that can be next in the path without
  creating a deadend. If making a neighboring room next in the path will create a deadend,
  it's coordinates are replaced with [-1 -1]."
  [g pos]
  (let [adjacent (neighbors g pos)
        deadends (set (find-deadends g adjacent))]
    (case (count deadends)
      0 adjacent
      1 (map #(if (deadends %) % [-1 -1]) adjacent)
      (repeat 4 [-1 -1]))))

;; cache of previously calculated answers
(def answers (atom {}))

(defn path-count
  "Returns the number of possible paths from intake to AC that cover all owned rooms."
  ([grid]
    (time
      (let [intake (find-room grid :in)]
        (do
          (swap! answers {}) ; reset the answer cache, so that multiple runs in the same repl session don't interfere with each other
          (path-count grid intake 0 (ans-length grid))))))
  ([grid [row col] depth ans-len]
    (let [pos [row col]
          room (get-in grid pos)
          length (inc depth)]
      (case room
        (:in :ow ) (let [g (assoc-in grid pos :vi)
                         ghead (assoc-in grid pos length)
                         prev-ans (get @answers ghead)]
                     (or
                       prev-ans
                       (let [count (if (connected? g)
                                     (apply + (map path-count (repeat g) (prune-deadends g pos) (repeat length) (repeat ans-len)))
                                     0)]
                         (do
                           (swap! answers assoc ghead count)
                           count))))
        :uo 0
        :vi 0
        :ac (if (= ans-len length)
              1
              0)
        0))))

;;;; I/O functions

(defn pp-grid
  "Prints out a nicely formatted grid."
  [g]
  (let [format-row (fn [r]
                      (str
                          "["
                          (apply str
                            (for [room r]
                              (format "%5s" room)))
                          " ]\n"))]
    (print
      (apply str (map format-row g)))))

(defn parse-layout [s]
  (let [raw (re-seq #"\w+" s)
        w (read-string (first raw))
        h (read-string (second raw))
        keys {"0" :ow
              "1" :uo
              "2" :in
              "3" :ac
              "4" :vi}
        key-seq (map keys (drop 2 raw))]
    (vec (map vec (partition w key-seq)))))

(defn load-small []
  (parse-layout (slurp "input/small.txt")))

(defn load-medium []
  (parse-layout (slurp "input/medium.txt")))

(defn load-impossible []
  (parse-layout (slurp "input/impossible.txt")))

(defn load-deadend []
  (parse-layout (slurp "input/deadend.txt")))

;;;; pre-load some grids for convenience at the repl

(def sm (load-small))
(def med (load-medium))
(def imp (load-impossible))
(def de (load-deadend))
