(ns quora.ptree
  (:require [clojure.string :as s]
            [clojure.set :as set]
            [clojure.pprint :as pp]))

(defn format-row [r]
  (str
    "["
    (apply str
      (for [room r]
        (format "%5s" room)))
    " ]\n"))

(defn pp-grid [g]
  (print
    (apply str (map format-row g))))

(defn parse-layout [s]
  (let [raw (re-seq #"\w+" s)
        w (read-string (first raw))
        h (read-string (second raw))
        keys {"0" :ow
              "1" :uo
              "2" :in
              "3" :ac}
        verbose-keys {"0" :owned
                      "1" :unowned
                      "2" :intake
                      "3" :ac}
        key-seq (map keys (drop 2 raw))]
    (vec (map vec (partition w key-seq)))))

(defn load-small []
  (parse-layout (slurp "input/small.txt")))

(defn load-medium []
  (parse-layout (slurp "input/medium.txt")))

(defn load-impossible []
  (parse-layout (slurp "input/impossible.txt")))

(defn ans-length [grid]
  (+ 2
    (count
      (filter #{:ow} (flatten grid)))))

(defn find-room [grid key]
  (first
    (filter #(not (neg? (get % 1)))
            (map vector
                 (range (count grid))
                 (map #(.indexOf % key) grid)))))

(defn mark-reachable [g pos]
  (let [[row col] pos
        room (get-in g pos)]
    (case room
      (:in :ac :ow ) (-> (assoc-in g pos :re )
                       (mark-reachable [row (dec col)])
                       (mark-reachable [row (inc col)])
                       (mark-reachable [(dec row) col])
                       (mark-reachable [(inc row) col]))
      g)))

(defn possible? [g]
  (let [intake (find-room g :ac)
        reachable (mark-reachable g intake)]
    (not
      (some #{:in :ac :ow } (flatten reachable)))))

(def failed (atom {}))

(defn already-failed? [g depth]
  (contains? (get @failed depth) g))

(defn build-node [grid [row col] depth ans-len]
  (let [pos [row col]
        room (get-in grid pos)
        length (inc depth)]
    (case room
      (:in :ow ) (let [g (assoc-in grid pos length)
                       child-pos [[row (dec col)]
                                  [row (inc col)]
                                  [(dec row) col]
                                  [(inc row) col]]
                       [left right up down] (pmap build-node (repeat g) child-pos (repeat length) (repeat ans-len))]
                   (if (or left right up down)
                     {:grid g
                      :pos pos
                      :left left
                      :right right
                      :up up
                      :down down
                      :length length}
                     nil))
      :uo nil
      :vi nil
      :ac (if (= ans-len length)
            {:grid (assoc-in grid pos length)
             :pos pos
             :left nil
             :right nil
             :up nil
             :down nil
             :length length}
            nil)
      nil)))

(defn path-tree [grid]
  (time
    (let [intake (find-room grid :in)]
      (build-node grid intake 0 (ans-length grid)))))

(defn leaf? [node]
  (and
    (nil? (:left node))
    (nil? (:right node))
    (nil? (:up node))
    (nil? (:down node))))

(defn find-paths [node]
  (if (nil? node)
    nil
    (if (leaf? node)
      (:path-grids node)
      (seq
        (concat
          (find-paths (:left node))
          (find-paths (:right node))
          (find-paths (:up node))
          (find-paths (:down node)))))))

(def sm (load-small))
(def sm-sol (path-tree sm))
(def med (load-medium))
(def imp (load-impossible))

