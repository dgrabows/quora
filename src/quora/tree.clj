(ns quora.tree
  (:require [clojure.string :as s]
            [clojure.set :as set]
            [clojure.pprint :as pp]))

(defn format-row [r]
  (str
    "["
    (apply str
      (for [room r]
        (format "%5s" room)))
    " ]\n"))

(defn pp-grid [g]
  (print
    (apply str (map format-row g))))

(defn parse-layout [s]
  (let [raw (re-seq #"\w+" s)
        w (read-string (first raw))
        h (read-string (second raw))
        keys {"0" :ow
              "1" :uo
              "2" :in
              "3" :ac
              "4" :vi}
        key-seq (map keys (drop 2 raw))]
    (vec (map vec (partition w key-seq)))))

(defn load-small []
  (parse-layout (slurp "input/small.txt")))

(defn load-medium []
  (parse-layout (slurp "input/medium.txt")))

(defn load-impossible []
  (parse-layout (slurp "input/impossible.txt")))

(defn load-deadend []
  (parse-layout (slurp "input/deadend.txt")))

(defn ans-length [grid]
  (+ 2
    (count
      (filter #{:ow} (flatten grid)))))

(defn find-room [grid key]
  (first
    (filter #(not (neg? (get % 1)))
            (map vector
                 (range (count grid))
                 (map #(.indexOf % key) grid)))))

(defn mark-reachable [g pos]
  (let [[row col] pos
        room (get-in g pos)]
    (case room
      (:in :ac :ow ) (-> (assoc-in g pos :re )
                       (mark-reachable [row (dec col)])
                       (mark-reachable [row (inc col)])
                       (mark-reachable [(dec row) col])
                       (mark-reachable [(inc row) col]))
      g)))

(defn possible? [g]
  (let [intake (find-room g :ac)
        reachable (mark-reachable g intake)]
    (not
      (some #{:in :ac :ow } (flatten reachable)))))

(def failed (atom {}))

(defn already-failed? [g depth]
  (contains? (get @failed depth) g))

(defn neighbors
  "Returns sequence of neighbor positions in left, right, up, down order."
  [g [row col]]
  [[row (dec col)]
   [row (inc col)]
   [(dec row) col]
   [(inc row) col]])

(defn deadend? [g pos]
  (and
    (= :ow (get-in g pos))
    (= 1 (count (filter #{:ow :in :ac} (map (partial get-in g) (neighbors g pos)))))))

(defn find-deadends [g positions]
  (filter (partial deadend? g) positions))

(defn prune-deadends [g pos]
  (let [adjacent (neighbors g pos)
        deadends (set (find-deadends g adjacent))]
    (case (count deadends)
      0 adjacent
      1 (map #(if (deadends %) % [-1 -1]) adjacent)
      (repeat 4 [-1 -1]))))

(defn build-node [grid [row col] path-grids depth ans-len]
  (let [pos [row col]
        room (get-in grid pos)
        length (inc depth)]
    (case room
      (:in :ow ) (let [g (assoc-in grid pos :vi)
                       ghead (assoc-in grid pos length)
                       pgrids (map #(assoc-in % pos length) path-grids)
                       [left right up down] (if (and
                                                  (not (already-failed? ghead length))
                                                  (possible? g))
                                              (map build-node (repeat g) (prune-deadends g pos) (repeat pgrids) (repeat length) (repeat ans-len))
                                              [nil nil nil nil])]
                   (if (or left right up down)
                     {:grid g
                      :path-grids pgrids
                      :pos pos
                      :left left
                      :right right
                      :up up
                      :down down
                      :length length}
                     (do
                       (swap! failed #(update-in % [length] set/union #{ghead}))
                       nil)))
      :uo nil
      :vi nil
      :ac (if (= ans-len length)
            {:grid (assoc-in grid pos :vi)
             :path-grids (map #(assoc-in % pos length) path-grids)
             :pos pos
             :left nil
             :right nil
             :up nil
             :down nil
             :length length}
            nil)
      nil)))

(defn path-tree [grid]
  (time
    (let [intake (find-room grid :in)]
      (do
        (swap! failed {})
        (build-node grid intake [grid] 0 (ans-length grid))))))

(defn leaf? [node]
  (and
    (nil? (:left node))
    (nil? (:right node))
    (nil? (:up node))
    (nil? (:down node))))

(defn find-paths [node]
  (if (nil? node)
    nil
    (if (leaf? node)
      (:path-grids node)
      (seq
        (concat
          (find-paths (:left node))
          (find-paths (:right node))
          (find-paths (:up node))
          (find-paths (:down node)))))))

(def sm (load-small))
(def med (load-medium))
(def imp (load-impossible))
(def de (load-deadend))
