(ns quora.matrix
  (:require [clojure.string :as s]
            [clojure.set :as set]
            [clojure.pprint :as pp]))

(defn format-row [r]
  (str
    "["
    (apply str
      (for [room r]
        (format "%5s" room)))
    " ]\n"))

(defn pp-grid [g]
  (print
    (apply str (map format-row g))))

(defn parse-layout [s]
  (let [raw (re-seq #"\w+" s)
        w (read-string (first raw))
        h (read-string (second raw))
        keys {"0" :ow
              "1" :uo
              "2" :in
              "3" :ac}
        verbose-keys {"0" :owned
                      "1" :unowned
                      "2" :intake
                      "3" :ac}
        key-seq (map keys (drop 2 raw))]
    (vec (map vec (partition w key-seq)))))

(defn load-small []
  (parse-layout (slurp "input/small.txt")))

(defn load-medium []
  (parse-layout (slurp "input/medium.txt")))

(defn load-impossible []
  (parse-layout (slurp "input/impossible.txt")))

(defn ans-length [grid]
  (+ 2
    (count
      (filter #{:ow} (flatten grid)))))

(defn find-room [grid key]
  (first
    (filter #(not (neg? (get % 1)))
            (map vector
                 (range (count grid))
                 (map #(.indexOf % key) grid)))))

(defn init-row [init-vals r]
  (vec (map init-vals r)))

;; grid structure:
;; - three-dimensional vector with first two dimensions representing position in the grid
;; - third dimension is the number of solutions that reach that position with that path length
;; - nil value means number of solutions for that position and path length has not been determined
;; - 0 means there are no possible solutions for that position and path length
;; - some positions and path lengths may be initialized to 0, such as unowned rooms, reaching the a/c unit without
;;   visiting all rooms, revisiting the intake room
(defn init-matrix-old [grid]
  (let [l (ans-length grid)
        init-vals {:in (vec (cons 1 (repeat (dec l) 0)))
                   :ow (vec (cons 0 (repeat (dec l) nil)))
                   :uo (vec (repeat l 0))
                   :ac (vec (concat (repeat (dec l) 0) [nil]))}]
    (vec (map (partial init-row init-vals) grid))))

;; grid structure:
;; - three-dimensional vector with first two dimensions representing position in the grid
;; - the third dimension references a set of solutions that reach the position with that path length
;; - each solution is represented as a vector of the positions visited to reach that position
;; - an empty set indicates that there are no possible solutions that reach that position with that path length
(defn init-matrix [grid]
  (let [l (ans-length grid)
        input (find-room grid :in)
        init-vals {:in (vec (cons #{[input]} (repeat (dec l) #{})))
                   :ow (vec (cons #{} (repeat (dec l) nil)))
                   :uo (vec (repeat l #{}))
                   :ac (vec (concat (repeat (dec l) #{}) [nil]))}]
    (vec (map (partial init-row init-vals) grid))))

(defn mat-of-depth [m n]
  (vec
    (map
      (fn [row] (vec (map #(count (nth % n)) row)))
      m)))

(defn sols-of-length [mat n]
  (remove
    nil?
    (for [row (range (count mat))
          col (range (count (mat 0)))]
      (if (>
            (count (get-in mat [row col n]))
            0)
        [row col]
        nil))))

(defn adjacent
  "Returns a sequence of unique rooms adjacent to the provided rooms."
  [mat rooms]
  (let [adj (fn [[r c]]
              [ [(inc r) c]
                [(dec r) c]
                [r (inc c)]
                [r (dec c)] ])]
    (filter
      #(get-in mat %)
      (set (apply concat (map adj rooms))))))

(defn room-sols-of-length [mat room n]
  (do
;    (printf "(room-sols-of-length mat %s %d)\n" room n)
    (set
      (apply
        concat
        (map
          (fn [[r c]]
            (map
              #(conj % room)
              (filter
                #(not-any? #{room} %)
                (get-in mat [r c (dec n)]))))
          (adjacent mat [room]))))))

(defn count-paths [grid]
  (time
    (last
      (let [m (atom (init-matrix grid))
            [ac-row ac-col] (find-room grid :ac)
            len (ans-length grid)]
        (for [n (rest (range len))
              :let [mat @m
                    prev-sols (sols-of-length mat (dec n))
                    cands (filter                           ; only get rooms w/o solutions for this path length
                            #(nil? (get-in mat (conj % n)))
                            (adjacent mat prev-sols))]]
          (do
;            (printf "cands(%d):\n" n)
;            (pp/pprint cands)
;            (pp-grid (mat-of-depth mat (dec n)))
;            (println)
;            (pp-grid (mat-of-depth mat n))
            (count
              (get-in
                (last
                  (for [[r c] cands
                        :let [sols (room-sols-of-length mat [r c] n)]]
                    (do
;                      (printf "sols:\n")
;                      (pp/pprint sols)
;                      (for [row (range (count mat))
;                            col (range (count (mat 0)))]
;                        (swap! m assoc-in [row col (dec n)] nil))
                      (swap! m assoc-in [r c n] sols))))
                [ac-row ac-col (dec len)]))))))))

(def sm (load-small))
(def med (load-medium))
(def imp (load-impossible))

(def mat-sm (init-matrix sm))