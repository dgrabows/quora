(defproject quora "0.1.0-SNAPSHOT"
  :description "Code from working on Quora datacenter cooling problem."
  :url "https://bitbucket.org/dgrabows/quora"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.3.0"]])
