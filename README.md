# Quora coding problem

This project contains code I've written while working on the
[Quora datacenter cooling challenge](http://www.quora.com/challenges#datacenter_cooling) for fun. Some of it works.
Some it is the result of failed ideas. The instructions under Usage will guide you towards a solution that I believe is
correct and reasonably efficient. It answers the larger example given in the challenge in just under two minutes on a
64-bit Windows 7 PC with a Core 2 Quad CPU running @ 2.83 GHz. It ran on a 32-bit 1.6 JVM.

The code is written in [Clojure](http://clojure.org/), but should not be taken as an example of either idiomatic Clojure
or good functional programming. Both are things I'm still very much in the process of internalizing.

## Usage

These are really general instructions that should probably be expanded in more detail.

1. Install [leiningen](https://github.com/technomancy/leiningen) if you don't already have it.
1. Start a repl by running "lein repl" from the project root.
1. Load the code for the quora.cooling namespace and switch to that namespace.

        user=> (load-file "src/quora/cooling.clj")
        #'quora.cooling/de
        user=> (in-ns 'quora.cooling)
        #<Namespace quora.cooling>
        quora.cooling=>

1. To find the solution for a layout, call the path-count function passing in the layout. The two layouts provided
in the challenge are pre-loaded in the quora.cooling namespace as sm (the 4 x 3 example) and med (the 7 x 8 example).

        quora.cooling=> (path-count sm)
        "Elapsed time: 40.094652 msecs"
        2
        quora.cooling=>

Notes:

* The default Java memory settings are fine for solving the small example. You will need to increase the max heap
  size when solving the larger example. I set the max heap size to 1 GB, but it only ended up expanding to ~500 MB with
  ~300 MB of that used.
* There are intellij project files included in the project. You will need intellij 11, along with the Leiningen and
  LaClojure plugins. The community edition of intellij should work fine.

## License

Copyright © 2012 Dan Grabowski

Distributed under the [Eclipse Public License](http://www.eclipse.org/legal/epl-v10.html).
